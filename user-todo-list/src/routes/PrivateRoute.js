import React from 'react'
import { Route, Redirect } from 'react-router-dom'
import { connect } from 'react-redux'

const PrivateRoute = ({ path, exact, component: Component, localStorage }) => (
    <Route path={path} exact={exact} render={(props) => {
        if (localStorage === 'true')
            return <Component {...props} />
        else
            return <Redirect to="/login" />
    }} />
)

const mapStateToProps = (state) => {
    return {
        localStorage: state.login.localStorage
    }
}

export default connect(mapStateToProps)(PrivateRoute)
