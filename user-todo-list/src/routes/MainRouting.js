import React from 'react'
import { BrowserRouter, Route, Redirect } from 'react-router-dom'
import UserPage from '../pages/Users/UsersPage'
import TodoPage from '../pages/Todo/TodoPage'
import LoginPage from '../pages/LoginPage'
import PrivateRoute from './PrivateRoute'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { isLogin, isLogout } from '../actions/LoginAction'

const MainRouting = (props) => {
    const { isLogin, isLogout } = props
    return (
        <BrowserRouter>
            <Route path="/processLogin" render={() => {
                isLogin()
                return <Redirect to="/users" />
            }} />

            <Route path="/processLogout" render={() => {
                isLogout()
                return <Redirect to="/login" />
            }} />

            <Route exact path="/" render={() => (
                <Redirect to="/login" />
            )} />

            <Route path="/login" component={LoginPage} exact={true} />
            <PrivateRoute path="/users" component={UserPage} exact={true} />
            <PrivateRoute path="/users/:user_id/todo" component={TodoPage} />
        </BrowserRouter>
    )
}

const mapStateToProps = (state) => {
    return {}
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({ isLogin, isLogout }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(MainRouting)
