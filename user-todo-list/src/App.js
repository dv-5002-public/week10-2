import React from 'react';
import './App.css';
import MainRouting from './routes/MainRouting';

function App() {
  return (
    <MainRouting />
  );
}

export default App;
