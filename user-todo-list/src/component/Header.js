import React from 'react'
import { PageHeader, Button, Icon } from 'antd'

const Header = (props) => {
    const { title } = props

    return (
        <PageHeader
            style={{
                borderRadius: "10px",
                backgroundColor: "white",
                marginBottom: '2%'
            }}
            onBack={() => window.history.back()}
            title={title}
            extra={[
                <Button key="logoutBtn" type="danger" onClick={() => window.location.pathname = "/processLogout"}>
                    <Icon type="logout" />Logout</Button>
            ]}
        />
    )
}

export default Header
