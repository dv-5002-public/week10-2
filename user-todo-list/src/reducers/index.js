import { combineReducers } from "redux"
import { TodoReducer } from "./TodoReducer"
import { UsersReducer } from "./UsersReducer"
import { LoginReducer } from "./LoginReducer"

export const rootReducer = combineReducers({
    todoState: TodoReducer,
    userState: UsersReducer,
    login: LoginReducer
})