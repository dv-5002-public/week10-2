import { LOGIN, LOGOUT } from "../actions/LoginAction"

const initialState = {
    localStorage: localStorage.getItem('isLogin')
}

export const LoginReducer = (state = initialState, action) => {
    switch (action.type) {
        case LOGIN:
            localStorage.setItem('isLogin', true)
            return {
                ...state,
                localStorage: localStorage.getItem('isLogin')
            }
        case LOGOUT:
            localStorage.setItem('isLogin', false)
            return {
                ...state,
                localStorage: localStorage.getItem('isLogin')
            }
        default:
            return state
    }
}