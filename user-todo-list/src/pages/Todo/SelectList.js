import React from 'react'
import { Select } from 'antd'
import { connect } from 'react-redux'
import { selectTodo } from '../../actions/TodoAction'
import { bindActionCreators } from 'redux'

const FilterList = props => {
    const { Option } = Select
    const { selectedTodoFilter, selectTodo } = props

    const handleChange = (value) => {
        selectTodo(value)
    }

    return (
        <Select value={selectedTodoFilter} style={{ width: '120px' }} onChange={handleChange}>
            <Option value={-1}>All</Option>
            <Option value={false}>Doing</Option>
            <Option value={true}>Done</Option>
        </Select>
    )
}

const mapStateToProp = (state) => {
    return {
        selectedTodoFilter: state.todoState.selectedTodoFilter
    }
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({ selectTodo }, dispatch)
}

export default connect(mapStateToProp, mapDispatchToProps)(FilterList)
