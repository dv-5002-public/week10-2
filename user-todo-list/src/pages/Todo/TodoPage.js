import React, { useEffect } from 'react'
import { Layout } from 'antd'
import Header from '../../component/Header'
import UserInfo from './UserInfo'
import TodoList from './TodoList'
import { connect } from 'react-redux'
import { fetchUser, fetchTodo } from '../../actions/TodoAction'
import { bindActionCreators } from 'redux'

const TodoPageRedux = (props) => {
    const userId = props.match.params.user_id
    const { fetchUser, fetchTodo } = props
    const { Content } = Layout

    useEffect(() => {
        fetchUser(userId)
        fetchTodo(userId)
    }, [])

    return (
        <Content style={{ padding: '50px 50px', minHeight: "100vh", background: "#EEEEEE" }}>
            <Header
                title="Todo List"
                onBackGoto="/users" />
            <UserInfo />
            <TodoList />
        </Content>
    )
}

const mapStateToProp = (state) => {
    return {}
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({ fetchUser, fetchTodo }, dispatch)
}

export default connect(mapStateToProp, mapDispatchToProps)(TodoPageRedux)
