import React from 'react'
import { connect } from 'react-redux'
import FilterList from './SelectList'
import { List, Typography, Button, Row, Icon } from 'antd'
import { doneTodo } from '../../actions/TodoAction'
import { bindActionCreators } from 'redux'
import SearchTodo from './SearchTodo'

const TodoList = (props) => {
    const { todoList, selectedTodoFilter, keyWord, doneTodo, isTodoLoading } = props

    const clickDone = (id) => {
        doneTodo(id)
    }

    return (
        <List
            loading={isTodoLoading}
            style={{
                backgroundColor: "white",
                borderRadius: '10px',
                borderColor:'white'
            }}
            header={
                <Row type="flex" justify="start">
                    <SearchTodo />
                    <FilterList />
                </Row>
            }
            bordered
            dataSource={
                selectedTodoFilter === -1 && keyWord === '' ?
                    todoList
                    :
                    selectedTodoFilter === -1 ?
                        todoList.filter(item => item.title.toUpperCase().match(keyWord.toUpperCase()))
                        :
                        todoList.filter(item => item.completed === selectedTodoFilter && item.title.toUpperCase().match(keyWord.toUpperCase()))
            }
            renderItem={(item, index) => (
                <List.Item>
                    <Typography.Text mark={!item.completed} delete={item.completed}>
                        {
                            item.completed ?
                                <Typography.Text delete>Doing</Typography.Text>
                                :
                                <Typography.Text mark>Done</Typography.Text>
                        }

                    </Typography.Text>

                    {" " + item.title}

                    {
                        !item.completed ?
                            <Button
                                value={index}
                                style={{ float: "right" }}
                                type="danger" ghost
                                shape="round"
                                icon="check-circle"
                                onClick={() => clickDone(item.id)}>
                                Done
                            </Button>
                            :
                            <div style={{ float: "right", color: 'green' }}><Icon type="check" /></div>
                    }
                </List.Item>
            )}
        />
    )
}

const mapStateToProps = (state) => {
    return {
        todoList: state.todoState.todoList,
        selectedTodoFilter: state.todoState.selectedTodoFilter,
        keyWord: state.todoState.keyWord,
        isTodoLoading: state.todoState.loadingTodo
    }
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({ doneTodo }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(TodoList)
