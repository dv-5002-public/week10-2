import React, { useEffect } from 'react'
import UsersList from './UsersList'
import { Icon, PageHeader, Button, Layout, BackTop } from 'antd'
import { connect } from 'react-redux'
import { fetchUsers } from '../../actions/UsersAction'
import { bindActionCreators } from 'redux'

const UserPage = (props) => {
    const { fetchUsers } = props
    const { Content } = Layout

    useEffect(() => {
        fetchUsers()
    }, [])

    return (
        <Content style={{ padding: '50px 100px', minHeight: "100vh", background: "#EEEEEE" }}>
            <PageHeader
                style={{
                    borderRadius: "10px",
                    marginBottom: '2%',
                    backgroundColor: "white"
                }}
                title="User List"
                extra={[
                    <Button
                        type="danger"
                        onClick={() => window.location.pathname = "/processLogout"}>
                        <Icon type="logout" />Logout</Button>
                ]} />
            <UsersList />
            <BackTop />
        </Content>
    )
}

const mapStateToProps = (state) => {
    return {}
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({ fetchUsers }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(UserPage)
