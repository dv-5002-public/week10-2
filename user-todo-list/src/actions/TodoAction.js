export const FETCH_TODO_BEGIN = 'FETCH_TODO_BEGIN'
export const FETCH_TODO_SUCCESS = 'FETCH_TODO_SUCCESS'
export const FETCH_TODO_ERROR = 'FETCH_TODO_ERROR'

export const FETCH_USER_BEGIN = 'FETCH_USER_BEGIN'
export const FETCH_USER_SUCCESS = 'FETCH_USER_SUCCESS'
export const FETCH_USER_ERROR = 'FETCH_USER_ERROR'

export const DONE_TODO = 'DONE_TODO'
export const SELECT_TODO = 'SELECT_TODO'
export const SEARCH_TODO = 'SEARCH_TODO'

export const fetchTodo = (userId) => {
    return dispatch => {
        dispatch(fetchTodoBegin())
        fetch('https://jsonplaceholder.typicode.com/todos?userId=' + userId)
            .then(response => response.json())
            .then(data => {
                dispatch(fetchTodoSuccess(data))
            })
            .catch(error => {
                alert(error)
                dispatch(fetchTodoError(error))
            })
    }
}

export const fetchTodoBegin = () => {
    return {
        type: FETCH_TODO_BEGIN
    }
}

export const fetchTodoSuccess = (todo) => {
    return {
        type: FETCH_TODO_SUCCESS,
        payLoad: todo
    }
}

export const fetchTodoError = (error) => {
    return {
        type: FETCH_TODO_ERROR,
        payLoad: error
    }
}

export const fetchUser = (userId) => {
    return dispatch => {
        dispatch(fetchUserBegin());
        fetch('https://jsonplaceholder.typicode.com/users/' + userId)
            .then(response => response.json())
            .then(data => {
                dispatch(fetchUserSuccess(data))
            })
            .catch(error => {
                alert(error);
                dispatch(fetchUserError(error))
            })
    }
}

export const fetchUserBegin = () => {
    return {
        type: FETCH_USER_BEGIN
    }
}

export const fetchUserSuccess = (user) => {
    return {
        type: FETCH_USER_SUCCESS,
        payLoad: user
    }
}

export const fetchUserError = (error) => {
    return {
        type: FETCH_USER_ERROR,
        payLoad: error
    }
}

export const doneTodo = (todoId) => {
    return {
        type: DONE_TODO,
        todoId
    }
}

export const selectTodo = (value) => {
    return {
        type: 'SELECT_TODO',
        value
    }
}

export const searchTodo = (keyWord) => {
    return {
        type: 'SEARCH_TODO',
        keyWord
    }
}
