export const LOGIN = 'LOGIN'
export const LOGOUT = 'LOGOUT'

export const isLogin = () => {
    return {
        type: LOGIN
    }
}

export const isLogout = () => {
    return {
        type: LOGOUT
    }
}